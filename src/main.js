import { createApp } from 'vue'
import App from './App.vue'
import './style.css'
import {Vue3Lottie} from "vue3-lottie";
import { createI18n } from 'vue-i18n';
import fr from '../public/i18n/fr.json'
import en from '../public/i18n/en.json'
import {createMemoryHistory, createRouter} from "vue-router";
import HomeView from "./components/views/HomeView.vue";
import JustMusicView from "./components/views/JustMusicView.vue";
import MovieFinderView from "./components/views/MovieFinderView.vue";
import AllinView from "./components/views/AllinView.vue";
import CompagnonView from "./components/views/CompagnonView.vue";


const routes = [
    { path: '/', component: HomeView },
    { path: '/moviefinder', component: MovieFinderView },
    { path: '/justmusic', component: JustMusicView },
    { path: '/allin', component: AllinView },
    { path: '/compagnon', component: CompagnonView },
]

const router = createRouter({
    history: createMemoryHistory(),
    routes,
    scrollBehavior(to, from, savedPosition) {
        return { top: 0 }
    },
})

const i18n = createI18n({
    locale: 'fr',
    fallbackLocale: 'fr',
    messages: {
        en: en,
        fr: fr
    }
})
createApp(App).use(Vue3Lottie).use(i18n).use(router).mount('#app')